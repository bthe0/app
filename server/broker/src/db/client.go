package db

import (
	"config"
	"db/models"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/stretchr/stew/objects"
)

var db *gorm.DB

func init() {
	dbConfig := objects.Map(config.Config.Store).Get("broker.dbConfig").(string)
	conn, err := gorm.Open("postgres", dbConfig)

	if err != nil {
		panic(err)
	}

	conn.BlockGlobalUpdate(true)
	db = conn
}

func GetPendingProjects() []models.Project {
	projects := []models.Project{}
	db.Limit(5).Where("processed = ?", 0).Find(&projects)
	return projects
}

func UpdateProjectStatus(id uint, status int) error {
	return db.Model(&models.Project{}).Where("id = ?", id).Update("processed", status).Error
}
