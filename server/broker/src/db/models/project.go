package models

import (
	"time"
)

type Project struct {
	ID uint `gorm:"primary_key"`
	Name string
	Processed int
	CreatedAt time.Time
	UpdatedAt time.Time
}