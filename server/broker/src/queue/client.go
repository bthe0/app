package queue

import (
	"config"
	"github.com/streadway/amqp"
	"github.com/stretchr/stew/objects"
)

func Connect() *amqp.Channel {
	queueName := objects.Map(config.Config.Store).Get("broker.queueName").(string)
	rabbitUrl := objects.Map(config.Config.Store).Get("broker.rabbitUrl").(string)

	conn, err := amqp.Dial(rabbitUrl)

	if err != nil {
		panic(err)
	}

	ch, err := conn.Channel()

	if err != nil {
		panic(err)
	}

	_, err = ch.QueueDeclare(
		queueName,
		true,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		panic(err)
	}

	return ch
}
