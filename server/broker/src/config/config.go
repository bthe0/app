package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"strings"
)

func GetEnv(key string, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}

	return fallback
}

var Config *Configuration

type Configuration struct {
	Store map[string]interface{}
}

func init() {
	path := strings.Join([]string{"../config/", GetEnv("ENV", "default"), ".json"}, "")
	f, err := ioutil.ReadFile(path)

	if err != nil {
		panic(err)
	}

	Config = &Configuration{}
	json.Unmarshal(f, &Config.Store)
}