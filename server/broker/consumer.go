package main

import (
	"config"
	"db"
	"github.com/stretchr/stew/objects"
	"log"
	"queue"
	"strconv"
)

func main() {
	ch := queue.Connect()

	queueName := objects.Map(config.Config.Store).Get("broker.queueName").(string)

	projects, err := ch.Consume(
		queueName,
		"",
		false,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		panic(err)
	}

	run := make(chan bool)

	go func() {
		for project := range projects {
			log.Printf("Received project with id: %s", project.Body)

			s := string(project.Body)
			id, err := strconv.Atoi(s)

			if err != nil {
				panic(err)
			}

			db.UpdateProjectStatus(uint(id), 2)
			project.Ack(false)
		}
	}()

	log.Printf("Consumer daemon has started.")
	<-run
}
