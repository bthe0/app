package main

import (
	"config"
	"db"
	"fmt"
	"github.com/streadway/amqp"
	"github.com/stretchr/stew/objects"
	"log"
	"queue"
	"time"
)

func main() {
	ch := queue.Connect()

	log.Println("Producer daemon has started.")
	queueName := objects.Map(config.Config.Store).Get("broker.queueName").(string)

	for range time.Tick(10 * time.Second) {
		projects := db.GetPendingProjects()

		for _, project := range projects {
			log.Printf("Adding %s to queue.", project.Name)

			err := db.UpdateProjectStatus(project.ID, 1)

			if err != nil {
				panic(err)
			}

			ch.Publish(
				"",
				queueName,
				false,
				false,
				amqp.Publishing{
					ContentType: "text/plain",
					Body: []byte(fmt.Sprint(project.ID)),
				})
		}
	}
}
