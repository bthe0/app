const { PROJECT_PENDING_PROCESSING } = require('../constants');

/**
 * The project model
 * @param sequelize
 * @param Types
 */

module.exports = (sequelize, Types) => {
     const Project = sequelize.define('Project', {
          id: {
               type: Types.INTEGER,
               allowNull: false,
               primaryKey: true,
               autoIncrement: true
          },
          name: {
               type: Types.STRING,
               allowNull: false
          },
          userId: {
               type: Types.INTEGER,
               field: 'user_id',
               references: {
                    model: 'users',
                    key: 'id'
               }
          },
          processed: {
               type: Types.INTEGER,
               defaultValue: PROJECT_PENDING_PROCESSING
          }
     }, {
          timestamps: true,
          underscored: true,
          tableName: 'projects',
          indexes: [
               {
                    fields: [
                         'user_id',
                         'processed'
                    ]
               }
          ]
     });
};