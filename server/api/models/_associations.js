/**
 * Create model associations
 * @param sequelize
 */

module.exports = (sequelize) => {
     const {Project, User} = sequelize.models;

     Project.belongsTo(User, {
          as: 'user'
     });
};