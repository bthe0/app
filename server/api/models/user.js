const { ACCESS_USER } = require('../constants');

/**
 * The user model
 * @param sequelize
 * @param Types
 */

module.exports = (sequelize, Types) => {
     const User = sequelize.define('User', {
          id: {
               type: Types.INTEGER,
               allowNull: false,
               primaryKey: true,
               autoIncrement: true
          },
          name: {
               type: Types.STRING,
               allowNull: false,
               validate: {
                    min: 1
               }
          },
          email: {
               type: Types.STRING,
               allowNull: false,
               isUnique: true,
               validate: {
                    isEmail: true
               }
          },
          password: {
               type: Types.STRING,
               allowNull: false
          },
          access: {
               type: Types.INTEGER,
               defaultValue: ACCESS_USER
          }
     }, {
          timestamps: true,
          underscored: true,
          paranoid: true,
          tableName: 'users',
          indexes: [
               {
                    unique: true,
                    fields: ['email']
               }
          ]
     });

     User.prototype.toJSON = function () {
          const values = Object.assign({}, this.get());
          delete values.password;
          return values;
     };
};