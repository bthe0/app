const aesjs = require('aes-js');

/**
 * AES Key
 * @type {number[]}
 */

const KEY_128 = [8, 6, 2, 3, 1, 6, 6, 1, 4, 5, 0, 4, 4, 0, 0, 4];

/**
 * Encryption middleware
 * @param enabled
 */

exports.parse = (enabled = true)  => {
     return (req, res, next) => {
          if (enabled) {
               let json = res.json;

               res.json = function(obj) {
                    const textBytes = aesjs.utils.utf8.toBytes(JSON.stringify(obj));
                    const aesCtr = new aesjs.ModeOfOperation.ctr(KEY_128, new aesjs.Counter(5));
                    const encryptedBytes = aesCtr.encrypt(textBytes);
                    const encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);

                    json.call(this, {
                         result: encryptedHex
                    });
               };
          }

          let data = '';

          req.on('data', chunk => data += chunk);

          req.on('end', () => {
               if (!data.length) {
                    return next();
               }

               if (!enabled) {
                    try {
                         req.body = JSON.parse(data);
                         next();
                    } catch (error) {
                         res.status(500).json({
                              errors: {
                                   message: ['Malformed request input']
                              }
                         });
                    }

                    return;
               }

               try {
                    const encryptedBytes = aesjs.utils.hex.toBytes(data);
                    const aesCtr = new aesjs.ModeOfOperation.ctr(KEY_128, new aesjs.Counter(5));
                    const decryptedBytes = aesCtr.decrypt(encryptedBytes);
                    const decryptedText = aesjs.utils.utf8.fromBytes(decryptedBytes);
                    req.body = JSON.parse(decryptedText);
                    next();
               } catch (error) {
                    res.status(500).json({
                         errors: {
                              message: ['Malformed request input']
                         }
                    });
               }
          });
     }
};