const rateLimit = require('express-rate-limit');

/**
 * Rate limiter middleware
 * @param maxRequests
 */

module.exports = (maxRequests = 100) => {
     return rateLimit({
          max: maxRequests,
          windowMs: 60 * 60 * 1000,
          message: {
               errors: {
                    message: ['Rate limit exceeded.']
               }
          }
     });
};