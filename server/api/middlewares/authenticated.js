const { ACCESS_USER } = '../constants';

/**
 * Middleware function for checking if user is authenticated
 * @param access
 */

module.exports = (access = ACCESS_USER) => {
     return (req, res, next) => {
          if (!req.user) {
               return res.status(401).json({
                    errors: {
                         message: ['User is not authenticated.']
                    }
               });
          }

          if (req.user && req.user.access < access) {
               return res.status(403).json({
                    errors: {
                         message: ['User is not allowed to do this action.']
                    }
               });
          }

          return next();
     };
};