/**
 * Project status types
 * @type {number}
 */

exports.PROJECT_NOT_PROCESSED = 0;
exports.PROJECT_PENDING_PROCESS = 1;
exports.PROJECT_PROCESSED = 2;

/**
 * User access levels
 * @type {number}
 */

exports.ACCESS_USER = 0;
exports.ACCESS_ADMIN = 1;