const authenticated = require('../middlewares/authenticated');
const limiter = require('../middlewares/limiter');

/**
 * The auth service
 * @param app
 */

module.exports = app => {
     app.post('/v1/auth/login', limiter(60), app.passport.authenticate('local', { failWithError: true }, null), (req, res) => {
          return res.json(req.user);
     });

     app.post('/v1/auth/logout', authenticated(), (req, res) => {
          req.logout();
          res.json({
               success: true
          });
     });
};