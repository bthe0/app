const {validate} = require('../core/validator');
const authenticated = require('../middlewares/authenticated');
const _ = require('lodash');
const {ACCESS_ADMIN} = require('../constants');

/**
 * Global context
 * @type {null}
 */

let context = null;

/**
 * Get projects route
 * @param req
 * @param res
 * @returns {Promise<void>}
 */

const getProjects = async (req, res) => {
     try {
          await validate(req.query, {
               page: 'required|numeric',
          });
     } catch (errors) {
          return res.status(406).json({ errors });
     }

     const {Project, User} = context.sequelize.models;
     const isAdmin = req.user.access === ACCESS_ADMIN;

     const filters = {};

     if (!isAdmin) {
          filters.userId = req.user.id;
     }

     if (req.query.search) {
          filters.name = {
               iLike: `%${req.query.search}%`
          };
     }

     const [
          data,
          total
     ] = await Promise.all([
          Project.findAll({
               limit: 5,
               offset: (req.query.page - 1) * 5,
               order: [
                    ['id', 'desc']
               ],
               where: filters,
               include: [{
                    model: User,
                    as: 'user',
                    attributes: [
                         'name'
                    ]
               }]
          }),
          Project.count({
               where: filters
          })
     ]);

     return res.json({
          data,
          total
     });
};

/**
 * Updates a project
 * @param req
 * @param res
 * @returns {Promise<void>}
 */

const updateProject = async (req, res) => {
     try {
          await validate(req.body, {
               name: 'required|min:1',
               processed: 'required|numeric'
          });
     } catch (errors) {
          return res.status(406).json({ errors });
     }

     const {id} = req.params;
     const {Project} = context.sequelize.models;

     const project = await Project.findById(id);

     if (!project) {
          return res.json({
               success: false
          });
     }

     const isAllowed = req.user.access === ACCESS_ADMIN || project.userId === req.user.id;

     if (!isAllowed) {
          return res.json({
               success: false
          });
     }

     const updated = await Project.update({
          name: req.body.name,
          processed: req.body.processed
     }, {
          where: {
               id
          }
     });

     return res.json({
          success: updated > 0
     });
};

/**
 * Removes a project
 * @param req
 * @param res
 * @returns {Promise<void>}
 */

const removeProject = async (req, res) => {
     const id = req.params.id;
     const {Project} = context.sequelize.models;

     const project = await Project.findById(id);

     if (!project) {
          return res.json({
               success: false
          });
     }

     const isAllowed = req.user.access === ACCESS_ADMIN || project.userId === req.user.id;

     if (!isAllowed) {
          return res.json({
               success: false
          });
     }

     const deleted = await Project.destroy({
          where: {
               id
          }
     });

     return res.json({
          success: deleted > 0
     });
};

/**
 * Creates a project
 * @param req
 * @param res
 * @returns {Promise<void>}
 */

const createProject = async (req, res) => {
     try {
          await validate(req.body, {
               name: 'required',
               processed: 'required|numeric'
          });
     } catch (errors) {
          return res.status(406).json({ errors });
     }

     const {Project} = context.sequelize.models;

     let project = await Project.create({
          ..._.pick(req.body, 'name', 'processed'),
          userId: req.user.id
     });

     project = project.toJSON();
     project.user = {
          name: req.user.name
     };

     return res.json(project);
};

/**
 * The project service
 * @param app
 */

module.exports = app => {
     context = app;

     app.get('/v1/projects', authenticated(), getProjects);
     app.post('/v1/projects', authenticated(),createProject);
     app.put('/v1/projects/:id(\\d+)', authenticated(), updateProject);
     app.delete('/v1/projects/:id(\\d+)', authenticated(), removeProject);
};