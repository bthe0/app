const Auth = require('./auth');
const User = require('./user');
const Project = require('./project');

/**
 * Load all the services
 * @param app
 */

module.exports = app => {
     Auth(app);
     User(app);
     Project(app);
};