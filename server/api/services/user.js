const {validate, registerRule} = require('../core/validator');
const authenticated = require('../middlewares/authenticated');
const limiter = require('../middlewares/limiter');
const bcrypt = require('bcrypt-nodejs');
const _ = require('lodash');
const Promise = require('bluebird');
const {ACCESS_ADMIN} = require('../constants');

const hashAsync = Promise.promisify(bcrypt.hash);

/**
 * The global context
 * @type {null}
 */

let context = null;

/**
 * After context is set
 */

const init = () => {
     registerRule('email_available', async (email, attribute, current, passes) => {
          const {User} = context.sequelize.models;

          const user = await User.findOne({
               where: {
                    email
               }
          });

          if (user) {
               return passes(false, 'Email already in use by another account.');
          }

          return passes();
     })
};

/**
 * Creates an user
 * @param req
 * @param res
 */

const createUser = async (req, res) => {
     try {
          await validate(req.body, {
               name: 'required',
               email: 'required|email|email_available',
               password: 'required',
               repeatPassword: 'required|same:password'
          });
     } catch (errors) {
          return res.status(406).json({ errors });
     }

     const {User} = context.sequelize.models;

     try {
          req.body.password = await hashAsync(req.body.password, null, null);
          const user = await User.create(_.pick(req.body, 'name', 'email', 'password'));

          return res.json(user);
     } catch(error) {
          return res.status(500).json({ errors: {} });
     }
};

/**
 * Lists the users from database
 * @param req
 * @param res
 * @returns {Promise<void>}
 */

const getUsers = async (req, res) => {
     try {
          await validate(req.query, {
               page: 'required|numeric'
          });
     } catch (errors) {
          return res.status(406).json({ errors });
     }

     const {User} = context.sequelize.models;

     const filters = {};

     if (req.query.search) {
          filters.name = {
               iLike: `%${req.query.search}%`
          };
     }

     const [
          data,
          total
     ] = await Promise.all([
          User.findAll({
               limit: 5,
               offset: (req.query.page - 1) * 5,
               order: [
                    ['id', 'desc']
               ],
               where: filters
          }),
          User.count({
               where: filters
          })
     ]);

     return res.json({
          data,
          total
     });
};

/**
 * Returns currently logged user
 * @param req
 * @param res
 * @returns {Promise<void>}
 */

const getUser = async (req, res) => {
     return res.json(req.user);
};

/**
 * Updates an user
 * @param req
 * @param res
 * @returns {Promise<void>}
 */

const updateUser = async (req, res) => {
     try {
          await validate(req.body, {
               access: 'required|numeric'
          });
     } catch (errors) {
          return res.status(406).json({ errors });
     }

     const id = req.params.id;
     const {User} = context.sequelize.models;

     const updated = await User.update({
          access: req.body.access
     }, {
          where: {
               id
          }
     });

     return res.json({
          success: updated > 0
     });
};

/**
 * Removes an user
 * @param req
 * @param res
 * @returns {Promise<void>}
 */

const removeUser = async (req, res) => {
     const id = req.params.id;
     const {User} = context.sequelize.models;

     const deleted = await User.destroy({
          where: {
               id
          }
     });

     return res.json({
          success: deleted > 0
     });
};

/**
 * The user service
 * @param app
 */

module.exports = app => {
     context = app;
     init();

     app.get('/v1/users', limiter(320), authenticated(ACCESS_ADMIN), getUsers);
     app.get('/v1/users/me', limiter(160), authenticated(), getUser);
     app.post('/v1/users', limiter(320), createUser);
     app.put('/v1/users/:id(\\d+)', limiter(320), authenticated(ACCESS_ADMIN), updateUser);
     app.delete('/v1/users/:id(\\d+)', limiter(320), authenticated(ACCESS_ADMIN), removeUser);
};