const redis = require('redis');

/**
 * Redis client configuration
 * @param app
 */

module.exports = app => {
     const client = redis.createClient(app.config.redis);




     /**
      * Bind redis object to global context
      */

     app.redis = client;
};