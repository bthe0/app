const winston = require('winston');

/**
 * Application logger
 * @type {winston.Logger}
 */

module.exports = winston.createLogger({
     level: 'info',
     format: winston.format.json(),
     transports: [
          new winston.transports.Console(),
     ]
});