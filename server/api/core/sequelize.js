const Sequelize = require('sequelize');

/**
 * Database connection handler
 * @param app
 */

module.exports = app => {
     const config = app.config.db;

     /**
      * Handle the connection
      * @type {module.exports|Sequelize}
      */

     const sequelize = new Sequelize(config.database, config.username, config.password, {
          host: config.host,
          port: config.port,
          dialect: 'postgres',
          operatorsAliases: Sequelize.Op,
          logging: config.debug ? console.log : null,
          pool: {
               max: 5,
               min: 0,
               idle: 10000
          }
     });

     /**
      * Load models
      */

     const models = [
          'project',
          'user',
          '_associations'
     ];

     for (const model of models) {
          require(`../models/${model}`)(sequelize, Sequelize.DataTypes);
     }

     /**
      * Bind database to app object
      */

     app.sequelize = sequelize;
};