const Validator = require('validatorjs');
const Promise = require('bluebird');

/**
 * Register custom validation rules
 * @param name
 * @param rule
 */

exports.registerRule = (name, rule) => {
     Validator.registerAsync(name, rule);
};

/**
 * Validator helper
 * @param body
 * @param rules
 */

exports.validate = (body, rules) => new Promise((resolve, reject) => {
     const validator = new Validator(body, rules);

     validator.fails(() => {
          reject(validator.errors.all())
     });

     validator.passes(resolve);
});
