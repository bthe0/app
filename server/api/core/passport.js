const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt-nodejs');
const Promise = require('bluebird');

const asyncCompare = Promise.promisify(bcrypt.compare);

/**
 * Setup passport
 * @param app
 */

module.exports = app => {
     /**
      * Local strategy setup
      */

     passport.use(new LocalStrategy({
          usernameField: 'email'
     }, async (email, password, done) => {
          const {User} = app.sequelize.models;

          try {
               const user = await User.findOne({
                    where: {
                         email
                    }
               });

               if (!user) {
                    return done({
                         status: 401,
                         message: 'Invalid credentials.'
                    }, false);
               }

               const validPassword = await asyncCompare(password, user.password);

               if (!validPassword) {
                    return done({
                         status: 401,
                         message: 'Invalid credentials.'
                    }, false);
               }

               return done(null, user);
          } catch(error) {
               return done(error, false);
          }
     }));

     passport.serializeUser((user, callback) => {
          callback(null, user.id);
     });

     passport.deserializeUser(async (id, done) => {
          const {User} = app.sequelize.models;

          try {
               const user = await User.findById(id);
               done(null, user);
          } catch(error) {
               done(error, null);
          }
     });

     app.use(passport.initialize());
     app.use(passport.session());

     /**
      * Bind the passport property to global object
      * @type {Authenticator|Passport|module.exports|*}
      */

     app.passport = passport;
};