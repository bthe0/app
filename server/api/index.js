const https = require('https');
const fs = require('fs');
const express = require('express');
const cors = require('cors');
const logger = require('./core/logger');
const helmet = require('helmet');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const RedisStore = require('connect-redis')(session);
const {GracefulShutdownManager} = require('@moebius/http-graceful-shutdown');
const crypto = require('./middlewares/crypto');

const app = express();

/**
 * Load the config
 * @type {string | string}
 */

const env = process.env.NODE_ENV || 'default';

try {
     app.config = require(`../config/${env}.json`).api;
} catch(e) {
     logger.error('Could not load the configuration file.', e);
     process.exit();
}

/**
 * Start the https server
 * @type {Server}
 */

app.server = https.createServer({
     key: fs.readFileSync(app.config.ssl.key),
     cert: fs.readFileSync(app.config.ssl.cert)
}, app);

const shutdownManager = new GracefulShutdownManager(app.server);

process.on('SIGTERM', () => {
     shutdownManager.terminate(() => logger.info('Closing remaining connections and shutting down gracefully.'));
});

process.on('SIGINT', () => {
     shutdownManager.terminate(() => logger.info('Closing remaining connections and shutting down gracefully.'));
});

logger.info(`Starting http server in ${env} mode.`);

require('./core/sequelize')(app);
require('./core/redis')(app);

app.enable('trust proxy');
app.use(cors({
     origin: 'https://localhost:3000',
     credentials: true
}));
app.use(helmet());

app.use(crypto.parse(app.config.encryptRequests));
app.use(cookieParser());
app.use(session({
     ...app.config.session,
     store: new RedisStore({
          client: app.redis,
          ttl: app.config.session.ttl * 60
     })
}));

require('./core/passport')(app);
require('./services')(app);

app.use((error, req, res, next) => {
     res.status(error.status || 500).json({
          errors: {
               message: [error.message]
          }
     });
});

app.server.listen(app.config.port, () => logger.info(`Server listening on port ${app.config.port}.`));