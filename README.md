### Project setup

1. `git clone`
2. `docker-compose up -d`
3. Interface is accessible on `https://localhost:3000`

### User accounts

1. Admin - admin@admin.com - password
2. User - user@user.com - password

### Others

1. Seeded database can be found within `misc/init` folder.
2. Make sure ports are open, else dependent services won't start.
