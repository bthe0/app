-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               PostgreSQL 10.5 (Debian 10.5-1.pgdg90+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 6.3.0-18+deb9u1) 6.3.0 20170516, 64-bit
-- Server OS:
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES  */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table public.projects
CREATE TABLE IF NOT EXISTS "projects" (
	"id" SERIAL,
	"name" CHARACTER VARYING(255) NOT NULL,
	"user_id" INTEGER NULL DEFAULT NULL,
	"processed" INTEGER NULL DEFAULT 0,
	"created_at" TIMESTAMP WITH TIME ZONE NOT NULL,
	"updated_at" TIMESTAMP WITH TIME ZONE NOT NULL,
	PRIMARY KEY ("id")
);

-- Dumping data for table public.projects: 5 rows
/*!40000 ALTER TABLE "projects" DISABLE KEYS */;
INSERT INTO "projects" ("id", "name", "user_id", "processed", "created_at", "updated_at") VALUES
	(21, 'User project', 10, 0, '2018-09-30 07:20:48.011+00', '2018-09-30 07:20:48.011+00'),
	(22, 'User second project', 10, 1, '2018-09-30 07:20:58.577+00', '2018-09-30 07:20:58.577+00'),
	(24, 'User third project', 10, 2, '2018-09-30 07:21:39.32+00', '2018-09-30 07:21:39.32+00'),
	(25, 'Admin first project', 9, 0, '2018-09-30 07:22:09.716+00', '2018-09-30 07:22:09.716+00'),
	(26, 'Admin second project', 9, 2, '2018-09-30 07:22:17.91+00', '2018-09-30 07:22:17.91+00');
/*!40000 ALTER TABLE "projects" ENABLE KEYS */;

-- Dumping structure for table public.users
CREATE TABLE IF NOT EXISTS "users" (
	"id" SERIAL,
	"name" CHARACTER VARYING(255) NOT NULL,
	"email" CHARACTER VARYING(255) NOT NULL,
	"password" CHARACTER VARYING(255) NOT NULL,
	"access" INTEGER NULL DEFAULT 0,
	"created_at" TIMESTAMP WITH TIME ZONE NOT NULL,
	"updated_at" TIMESTAMP WITH TIME ZONE NOT NULL,
	"deleted_at" TIMESTAMP WITH TIME ZONE NULL DEFAULT NULL,
	PRIMARY KEY ("id")
);

-- Dumping data for table public.users: 2 rows
/*!40000 ALTER TABLE "users" DISABLE KEYS */;
INSERT INTO "users" ("id", "name", "email", "password", "access", "created_at", "updated_at", "deleted_at") VALUES
	(9, 'Admin', 'admin@admin.com', '$2a$10$L0tRYG78P.9Sfyaz42p..eoOKReMTureZE7/7T48AkEm0noooiFjG', 1, '2018-09-30 07:20:04.621+00', '2018-09-30 07:20:04.621+00', NULL),
	(10, 'User', 'user@user.com', '$2a$10$Utta0vNrM87qq1mo96wMzOQzpSNxfnWUs6TJwEZoUGA4WHNTCBm5O', 0, '2018-09-30 07:20:42.14+00', '2018-09-30 07:20:42.14+00', NULL);
/*!40000 ALTER TABLE "users" ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
