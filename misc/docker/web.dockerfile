FROM tiangolo/node-frontend:10 as build-stage
WORKDIR /app
COPY client/package*.json /app/
RUN npm install
COPY ./client /app/
RUN npm run build

FROM nginx:1.15
COPY --from=build-stage /app/build/ /usr/share/nginx/html
COPY ./misc/conf/web.nginx.conf /etc/nginx/conf.d/default.conf
COPY ./misc/ssl/* /etc/ssl/