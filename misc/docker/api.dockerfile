FROM node:10
WORKDIR /usr/src/app

COPY . .
EXPOSE 3030
WORKDIR /usr/src/app/server/api

RUN npm install --only=prod
CMD ["npm", "start"]