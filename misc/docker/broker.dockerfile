FROM golang:latest
RUN mkdir /app
COPY ./server/broker/ /app/
COPY ./server/config /config/

WORKDIR /app
ENV GOPATH /app
ENV ENV "production"

RUN go get -u github.com/stretchr/stew/objects
RUN go get -u github.com/jinzhu/gorm/dialects/postgres
RUN go get -u github.com/jinzhu/gorm
RUN go get -u github.com/streadway/amqp

RUN go build producer.go
RUN go build consumer.go