import React, {Component} from 'react';

import {Route, Switch} from 'react-router';
import {BrowserRouter as Router} from 'react-router-dom';

import Async from './Misc/Async';
import Guard from './Misc/Guard';

import { ACCESS_ADMIN } from '../constants';

export default class Root extends Component {
     render() {
         return (
              <Router>
                   <Switch>
                        {/*Auth routes*/}
                        <Route path={'/auth'} exact component={Guard(Async('Auth'), {redirectSuccess: '/'})}/>
                        <Route path={`/auth/register`} exact component={Guard(Async('Auth/Register'), {redirectSuccess: '/'})}/>
                        {/*Panel routes*/}
                        <Route path={'/'} exact component={Guard(Async('Panel/Projects'), {redirectFailed: '/auth'})} />
                        <Route path={`/users`} exact component={Guard(Async('Panel/Users'), {redirectFailed: '/auth', accessRole: ACCESS_ADMIN})}/>
                        {/*Other routes*/}
                        <Route path={`*`} component={Async('Misc/NotFound')}/>
                   </Switch>
              </Router>
         );
     }
}
