import React, {Component} from 'react';
import {buildError, setTitle} from '../../lib/util';
import {Button, Form, FormGroup, Input, Label, Container, Row, Col, Alert} from 'reactstrap';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import http from '../../lib/http';
import pick from 'lodash/pick';
import get from 'lodash/get';

export default class Register extends Component {
     state = {
          loading: false,
          error: false,
          values: {
               name: '',
               password: '',
               repeatPassword: '',
               email: ''
          }
     };

     static contextTypes = {
          router: PropTypes.object.isRequired
     };

     componentDidMount() {
          setTitle('Sign up')
     }

     register = async () => {
          const {history} = this.context.router;
          const {values} = this.state;

          if (values.repeatPassword !== values.password) {
               return this.setState({
                    error: 'Repeated password does not match the first password.'
               });
          }

          try {
               await http.post('users', values);
          } catch(error) {
               return this.setState({
                    error: buildError(get(error, 'response.data.errors', true))
               });
          }

          try {
               await http.post('auth/login', pick(values, 'email', 'password'));
               history.push('/');
          } catch(error) {
               this.setState({
                    error: buildError(get(error, 'response.data.errors', true))
               });
          }
     };

     onChange = e => {
          const {name, value} = e.target;
          const {values} = this.state;

          this.setState({
               values: {
                    ...values,
                    [name]: value
               }
          });
     };

     onKeyPress = async e => {
          const {values, loading} = this.state;
          const disabled = !values.name || !values.password || !values.repeatPassword || !values.email || loading;

          if (e.key === 'Enter' && !disabled) {
               await this.register(e);
          }
     };

     closeAlert = () => {
          this.setState({
               error: false
          });
     };

     render() {
          const {error, loading, values} = this.state;
          const disabled = !values.name || !values.password || !values.repeatPassword || !values.email || loading;

          return (
               <Container>
                    <Row>
                         <Col xs={12} lg={{ size: 4, offset: 4}} className={'mt-120'}>
                              <Form method={'POST'}>
                                   <div className={'text-center'}>
                                        <h3>Sign Up</h3>
                                        <small>Already have an account? <Link to={`/auth`}>Sign in</Link> instead</small>
                                   </div>
                                   <hr/>
                                   {
                                        error && (
                                             <Alert color={'danger'} toggle={this.closeAlert}>
                                                  {error}
                                             </Alert>
                                        )
                                   }
                                   <FormGroup>
                                        <Label for="email">Name</Label>
                                        <Input type="text" name="name" id="name" placeholder="John Doe" autoFocus={true} onChange={this.onChange} onKeyPress={this.onKeyPress}/>
                                   </FormGroup>
                                   <FormGroup>
                                        <Label for="email">Email</Label>
                                        <Input type="email" name="email" id="email" placeholder="me@example.com" onChange={this.onChange} onKeyPress={this.onKeyPress}/>
                                   </FormGroup>
                                   <FormGroup>
                                        <Label for="password">Password</Label>
                                        <Input type="password" name="password" id="password" placeholder="•••••••••" onChange={this.onChange} onKeyPress={this.onKeyPress}/>
                                   </FormGroup>
                                   <FormGroup>
                                        <Label for="password">Repeat Password</Label>
                                        <Input type="password" name="repeatPassword" id="repeatPassword" placeholder="•••••••••" onChange={this.onChange} onKeyPress={this.onKeyPress}/>
                                   </FormGroup>
                                   <Button color={`primary`} block disabled={disabled} onClick={this.register}>Register</Button>
                              </Form>
                         </Col>
                    </Row>
               </Container>
          );
     }
}