import React, {Component} from 'react';
import {buildError, setTitle} from '../../lib/util';
import {Button, Form, FormGroup, Input, Label, Container, Row, Col, Alert} from 'reactstrap';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import http from '../../lib/http';
import get from 'lodash/get';

export default class Auth extends Component {
     state = {
          loading: false,
          error: false,
          values: {
               email: '',
               password: ''
          }
     };

     static contextTypes = {
          router: PropTypes.object.isRequired
     };

     componentDidMount() {
          setTitle('Sign in');
     }

     /**
      * The login action
      * @param e
      * @returns {Promise<void>}
      */

     login = async e => {
          e.preventDefault();

          const {history} = this.context.router;
          const {values} = this.state;

          this.setState({
               loading: true
          });

          try {
               await http.post('auth/login', values);
               history.push('/');
          } catch(error) {
               this.setState({
                    error: buildError(get(error, 'response.data.errors'), true)
               });
          }

          this.setState({
               loading: false
          });
     };

     onChange = e => {
          const {name, value} = e.target;
          const {values} = this.state;

          this.setState({
               values: {
                    ...values,
                    [name]: value
               }
          });
     };

     closeAlert = () => {
          this.setState({
               error: false
          });
     };

     onKeyPress = async e => {
          const {values, loading} = this.state;
          const disabled = !values.email || !values.password || loading;

          if (e.key === 'Enter' && !disabled) {
               await this.login(e);
          }
     };

     render() {
          const {values, loading, error} = this.state;
          const disabled = !values.email || !values.password || loading;

          return (
               <Container>
                    <Row>
                         <Col xs={12} lg={{ size: 4, offset: 4}} className={'mt-120'}>
                              <Form method={'POST'}>
                                   <div className={'text-center'}>
                                        <h3>Sign In</h3>
                                        <small>Don't have an account? <Link to={`/auth/register`}>Register</Link> instead</small>
                                   </div>
                                   <hr/>
                                   {
                                        error && (
                                             <Alert color={'danger'} toggle={this.closeAlert}>
                                                  {error}
                                             </Alert>
                                        )
                                   }
                                   <FormGroup>
                                        <Label for="email">Email</Label>
                                        <Input type="email" name="email" id="email" placeholder="me@example.com"
                                               onKeyPress={this.onKeyPress}
                                               autoFocus={true} value={values.email} onChange={this.onChange}/>
                                   </FormGroup>
                                   <FormGroup>
                                        <Label for="password">Password</Label>
                                        <Input type="password" name="password" id="password" placeholder="•••••••••"
                                               onKeyPress={this.onKeyPress} onChange={this.onChange}
                                               value={values.password}/>
                                   </FormGroup>
                                   <Button color={'primary'} block disabled={disabled} onClick={this.login}>Login</Button>
                              </Form>
                         </Col>
                    </Row>
               </Container>
          );
     }
}