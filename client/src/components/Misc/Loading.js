import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class Loading extends Component {
     static propTypes = {
          visible: PropTypes.bool.isRequired
     };

     render() {
          const {visible} = this.props;

          if (!visible) {
               return null;
          }

          return (
               <div className={'loading'}>
                    <div className="lds-ripple">
                         <div></div>
                         <div></div>
                    </div>
               </div>
          );
     }
}