import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import {Button} from 'reactstrap';
import http from '../../lib/http';
import {ACCESS_ADMIN} from '../../constants';
import PropTypes from 'prop-types';

export default class Menu extends Component {
     static contextTypes = {
          router: PropTypes.object.isRequired
     };

     logout = async () => {
          const {history} = this.context.router;

          try {
               const res = await http.post('/auth/logout');

               if (res.data && res.data.success) {
                    history.push('/auth');
               }
          } catch(error) {

          }
     };

     _getAvatarContent = (name) => {
          return name && name.split(' ').map(word => word[0].toUpperCase()).join('');
     };

     render() {
          const {user} = this.props;

          return (
               <div className={'menu'}>
                    <div className={'user-profile'}>
                         <div className={'avatar'}>{this._getAvatarContent(user.name)}</div>
                         <div>
                              <div className={'name'}>{user.name}</div>
                              <Button color={`danger`} size={'sm'} onClick={this.logout} outline>Logout</Button>
                         </div>
                    </div>
                    <h6>
                         <i className={'fas fa-bars'}/> Menu
                    </h6>
                    <hr/>
                    <ul className={'list-unstyled'}>
                         <li>
                              <NavLink to={`/`} exact>
                                   <i className={'fas fa-book'}/> Projects
                              </NavLink>
                         </li>
                         {
                              user.access === ACCESS_ADMIN && (
                                   <li>
                                        <NavLink to={`/users`}>
                                             <i className={'fas fa-users'}/> Users
                                        </NavLink>
                                   </li>
                              )
                         }
                    </ul>
               </div>

          );
     }
}