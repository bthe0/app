import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Container, Row, Col} from 'reactstrap';
import Menu from './Menu';
import Context from './Context';

export default class Layout extends Component {
     static propTypes = {
          children: PropTypes.oneOfType([
               PropTypes.arrayOf(PropTypes.node),
               PropTypes.node
          ]).isRequired
     };

     render() {
          const {children} = this.props;

          return (
               <div className={`layout`}>
                    <Container>
                         <Row>
                              <Col xs={12} sm={3}>
                                   <Context.Consumer>
                                        {
                                             ({ user }) => (
                                                  <Menu user={user}/>
                                             )
                                        }
                                   </Context.Consumer>
                              </Col>
                              <Col xs={12} sm={9}>
                                   {children}
                              </Col>
                         </Row>
                    </Container>
               </div>
          );
     }
}