import React, {Component} from 'react';
import Loading from './Loading';

/**
 * HoC handling lazy loading components
 * @param path
 * @param options
 * @returns {{new(): {state, componentDidMount(), render(): *}, prototype: {state, componentDidMount(), render(): *}}}
 */

export default (path) => {
     return class Async extends Component {
          static Component = null;

          state = {
               LoadedComponent: Async.Component
          };

          async componentWillMount() {
               const {LoadedComponent} = this.state;

               if (!LoadedComponent) {
                    const Loaded = await import(`../${path}`);
                    Async.Component = Loaded.default;

                    this.setState({
                         LoadedComponent: Loaded.default
                    });
               }
          }
          render() {
               const {LoadedComponent} = this.state;

               if (LoadedComponent) {
                    return <LoadedComponent {...this.props} />
               }

               return <Loading visible={true}/>;
          }
     }
};