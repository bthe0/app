import React, {Component, Fragment} from 'react';
import {setTitle} from '../../../lib/util';
import Layout from "../../Misc/Layout";
import confirm from 'reactstrap-confirm';
import {Alert, Table, Input, Button, Dropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap';
import http from '../../../lib/http';
import Context from '../../Misc/Context';
import Loading from '../../Misc/Loading';

export default class Users extends Component {
     state = {
          action: false,
          users: [],
          page: 1,
          loading: true,
          hasMoreItems: false,
          filters: {}
     };

     async componentDidMount() {
          setTitle('Users');
          await this.loadData(1);
     }

     /**
      * Loads the data from the Api
      * @param page
      * @param filters
      * @returns {Promise<void>}
      * @private
      */

     loadData = async (page, filters = {}) => {
          const {users} = this.state;

          this.setState({
               loading: true
          });

          try {
               const res = await http.get('users', {
                    params: {
                         page,
                         ...filters
                    }
               });

               if (res.data && res.data.errors) {
                    throw new Error('Failed to load users.');
               }

               if (res.data) {
                    this.setState({
                         users: page === 1 ? res.data.data : [...users, ...res.data.data],
                         hasMoreItems: res.data.total - 5 > users.length,
                         page,
                         filters
                    });
               }
          } catch(error) {
               this.setState({
                    action: {
                         level: 'danger',
                         text: 'An error occurred while loading users.'
                    }
               });
          }

          this.setState({
               loading: false
          });
     };

     /**
      * Toggles actions drop down for a project
      * @param user
      */

     toggle = user => {
          const {users} = this.state;

          this.setState({
               users: users.map(oldUser => {
                    if(user.id === oldUser.id) {
                         oldUser.toggled = !oldUser.toggled;
                    }

                    return oldUser;
               })
          });
     };

     /**
      * Handles delete user action
      * @param user
      * @returns {Promise<void>}
      */

     delete = async user => {
          let {users} = this.state;
          const result = await confirm({
               message: 'Are you sure you want to delete this entry?',
               title: 'Confirm Action',
               confirmText: 'Remove',
               confirmColor: 'danger'
          });

          if (!result) {
               return;
          }

          let action = {};

          try {
               const res = await http.delete(`users/${user.id}`);

               if (res.data && res.data.success) {
                    users = users.filter(item => item.id !== user.id);

                    action = {
                         level: 'success',
                         text: 'User successfully deleted'
                    };
               }

          } catch(error) {
               action = {
                    level: 'danger',
                    text: 'Error occurred while deleting user'
               };
          }

          this.setState({ action, users });
     };

     /**
      * Handles user's access update
      * @param user
      * @param access
      * @returns {Promise<void>}
      */

     updateAccess = async (user, access) => {
          let {users} = this.state;
          let action = {};

          try {
               const res = await http.put(`users/${user.id}`, {
                    access
               });

               if (res.data && res.data.success) {
                    users = users.map(oldUser => {
                         if (oldUser.id === user.id) {
                              oldUser.access = access;
                         }

                         return oldUser;
                    });

                    action = {
                         level: 'success',
                         text: 'User successfully updated'
                    };
               }

          } catch(error) {
               action = {
                    level: 'danger',
                    text: 'Error occurred while updating user'
               };
          }

          this.setState({ action, users });
     };

     /**
      * Closes the action alert
      */

     closeAlert = () => {
          this.setState({
               action: false
          });
     };

     /**
      * On search user input
      * @param e
      */

     onSearch = e => {
          const {searchTimeout} = this.state;
          const {value} = e.target;

          if (searchTimeout) {
               clearTimeout(searchTimeout)
          }

          this.setState({
               searchTimeout: setTimeout(async () => {
                    await this.loadData(1, { search: value });
               }, 350)
          });
     };

     render() {
          const {users, action, page, hasMoreItems, loading, filters} = this.state;

          return (
               <Layout>
                    <div className={'flex-center-sm mt-23-xs'}>
                         <h4>Users</h4>
                         <div className={'ml-auto flex-center'}>
                              <div className={'input-search'}>
                                   <Input placeholder="Search" onChange={this.onSearch}/>
                                   <i className={'fas fa-search'}/>
                              </div>
                         </div>
                    </div>
                    <hr className={'mb-33'}/>
                    {
                         action && (
                              <Alert color={action.level} toggle={this.closeAlert}>
                                   {action.text}
                              </Alert>
                         )
                    }
                    <Loading visible={loading}/>
                    {
                         !Boolean(users.length) && !loading && (
                              <div>
                                   <p>There are no users to display</p>
                              </div>
                         )
                    }
                    {
                         Boolean(users.length) && !loading && (
                              <Fragment>
                                   <Table borderless striped={true} responsive={true}>
                                        <thead>
                                        <tr>
                                             <th>#</th>
                                             <th>Name</th>
                                             <th>Email</th>
                                             <th>Access</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                             users.map((user, key) => (
                                                  <tr key={key}>
                                                       <th scope="row">{key + 1}</th>
                                                       <td>{user.name}</td>
                                                       <td>{user.email}</td>
                                                       <td>
                                                            <Context.Consumer>
                                                                 {
                                                                      ({ user: currentUser }) => (
                                                                           <Input type={'select'} className={'access-select'}
                                                                                  value={user.access}
                                                                                  onChange={e => this.updateAccess(user, e.target.value)}
                                                                                  disabled={user.id === currentUser.id}>
                                                                                <option value={0}>User</option>
                                                                                <option value={1}>Admin</option>
                                                                           </Input>
                                                                      )
                                                                 }
                                                            </Context.Consumer>
                                                       </td>
                                                       <td>
                                                            <Context.Consumer>
                                                                 {
                                                                      ({ user: currentUser }) => {
                                                                           if (currentUser.id === user.id) {
                                                                                return (
                                                                                     <div className={'actions-wrapper'}>
                                                                                          <i className="fas fa-ellipsis-h actions"/>
                                                                                     </div>
                                                                                );
                                                                           }

                                                                           return (
                                                                                <Dropdown isOpen={user.toggled} toggle={e => this.toggle(user)} className={'actions-wrapper'}>
                                                                                     <DropdownToggle>
                                                                                          <i className="fas fa-ellipsis-h actions"/>
                                                                                     </DropdownToggle>
                                                                                     <DropdownMenu>
                                                                                          <DropdownItem onClick={() => this.delete(user)}>
                                                                                               <i className="fas fa-trash-alt"/> Delete
                                                                                          </DropdownItem>
                                                                                     </DropdownMenu>
                                                                                </Dropdown>
                                                                           );
                                                                      }
                                                                 }
                                                            </Context.Consumer>
                                                       </td>
                                                  </tr>
                                             ))
                                        }
                                        </tbody>
                                   </Table>
                                   {
                                        hasMoreItems && (
                                             <div className={'text-center'}>
                                                  <Button color={`primary`} size={'sm'} className={'mt-23'} onClick={() => this.loadData(page + 1, filters)}>Load more</Button>
                                             </div>
                                        )
                                   }
                              </Fragment>
                         )
                    }
               </Layout>
          );
     }
}