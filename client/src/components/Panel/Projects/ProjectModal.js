import React, {Component} from 'react';
import {Modal, ModalBody, ModalFooter, ModalHeader, Button, FormGroup, Label, Input, Form} from 'reactstrap';
import http from '../../../lib/http';
import PropTypes from 'prop-types';

export default class ProjectModal extends Component {
     state = {
          open: false,
          project: false,
          values: {
               name: '',
               processed: 0
          }
     };

     static propTypes = {
          onAction: PropTypes.func.isRequired
     };

     /**
      * Toggles the project modal
      * @param open
      * @param project
      */

     toggle = (open, project = false) => {
          this.setState({
               open,
               project,
               values: {
                    name: project ? project.name : '',
                    processed: project ? project.processed : 0
               }
          });
     };

     /**
      * When a field's value changes
      * @param e
      */

     onChange = e => {
          const {values} = this.state;
          const {name, value} = e.target;

          this.setState({
               values: {
                    ...values,
                    [name]: value
               }
          });
     };

     /**
      * Handles modal's action
      * @param e
      */

     handle = async e => {
          const {onAction} = this.props;
          const {values, project} = this.state;
          e.preventDefault();

          try {
               if (!project) {
                    const res = await http.post('projects', values);

                    if (res.data) {
                         onAction(res.data, 'add');
                         this.toggle(false);
                         return;
                    }
               }

               const res = await http.put(`projects/${project.id}`, values);

               if (res.data && res.data.success) {
                    onAction({...project, ...values}, 'edit');
                    this.toggle(false);
               }
          } catch(error) {
               onAction(null, null, true);
          }
     };

     onKeyPress = async e => {
          const {values} = this.state;
          const disabled = !values.name;

          if (e.key === 'Enter' && !disabled) {
               await this.handle(e);
          }
     };

     render() {
          const {open, project, values} = this.state;
          const disabled = !values.name;

          return (
               <Modal isOpen={open} autoFocus={false}>
                    <ModalHeader toggle={() => this.toggle(false)}>{project ? 'Edit Project' : 'Add Project'}</ModalHeader>
                    <Form method={'POST'}>
                         <ModalBody>
                              <FormGroup>
                                   <Label for="name">Name</Label>
                                   <Input type="text" name="name" id="name" placeholder="Project name" autoFocus={true} value={values.name} onChange={this.onChange} onKeyPress={this.onKeyPress}/>
                              </FormGroup>
                              <FormGroup>
                                   <Label for="password">Processed</Label>
                                   <Input type={'select'} name={'processed'} value={values.processed} onChange={this.onChange}>
                                        <option value={0}>Not processed</option>
                                        <option value={1}>Pending processed</option>
                                        <option value={2}>Processed</option>
                                   </Input>
                              </FormGroup>
                         </ModalBody>
                         <ModalFooter>
                              <Button onClick={() => this.toggle(false)} color={''}>Cancel</Button>
                              <Button color="primary" disabled={disabled} onClick={this.handle}>Save</Button>{' '}
                         </ModalFooter>
                    </Form>
               </Modal>
          );
     }
}