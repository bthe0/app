import React, {Component, Fragment} from 'react';
import {setTitle} from '../../../lib/util';
import Layout from '../../Misc/Layout';
import ProjectModal from './ProjectModal';
import confirm from 'reactstrap-confirm';
import http from '../../../lib/http';
import Context from '../../Misc/Context';
import {ACCESS_ADMIN, PROJECT_NOT_PROCESSED, PROJECT_PENDING_PROCESS, PROJECT_PROCESSED} from '../../../constants';
import Loading from '../../Misc/Loading';
import get from 'lodash/get';
import {
     Table,
     Button,
     Dropdown,
     DropdownToggle,
     DropdownMenu,
     DropdownItem,
     Badge,
     Alert,
     Input
} from 'reactstrap';

export default class Projects extends Component {
     state = {
          action: false,
          loading: true,
          projects: [],
          searchTimeout: false,
          filters: {}
     };

     async componentDidMount() {
          setTitle('Projects');
          await this.loadData(1);
     }

     /**
      * Loads the data from the api
      * @param page
      * @param filters
      * @returns {Promise<void>}
      * @private
      */

     loadData = async (page = 1, filters = {}) => {
          const {projects} = this.state;

          this.setState({
               loading: true
          });

          try {
               const res = await http.get('projects', {
                    params: {
                         page,
                         ...filters
                    }
               });

               if (res.data && res.data.errors) {
                    throw new Error('Failed to load projects.');
               }

               if (res.data) {
                    this.setState({
                         projects: page === 1 ? res.data.data : [...projects, ...res.data.data],
                         hasMoreItems: res.data.total - 5 > projects.length,
                         page,
                         filters
                    });
               }
          } catch(error) {
               this.setState({
                    action: {
                         level: 'danger',
                         text: 'An error occurred while loading projects.'
                    }
               });
          }

          this.setState({
               loading: false
          });
     };

     /**
      * Toggles actions drop down for a project
      * @param project
      */

     toggle = project => {
          const {projects} = this.state;

          this.setState({
               projects: projects.map(oldProject => {
                    if(project.id === oldProject.id) {
                         oldProject.toggled = !oldProject.toggled;
                    }

                    return oldProject;
               })
          });
     };

     /**
      * Handles delete project action
      * @param project
      * @returns {Promise<void>}
      */

     delete = async project => {
          let {projects} = this.state;

          const result = await confirm({
               message: 'Are you sure you want to delete this entry?',
               title: 'Confirm Action',
               confirmText: 'Remove',
               confirmColor: 'danger'
          });

          if (!result) {
               return;
          }

          let action = {};

          try {
               const res = await http.delete(`projects/${project.id}`);

               if (res.data && res.data.success) {
                    projects = projects.filter(item => item.id !== project.id);

                    action = {
                         level: 'success',
                         text: 'User successfully deleted'
                    };
               }

          } catch(error) {
               action = {
                    level: 'danger',
                    text: 'Error occurred while deleting user'
               };
          }

          this.setState({projects, action});
     };

     /**
      * Closes the action alert
      */

     closeAlert = () => {
          this.setState({
               action: false
          });
     };


     /**
      * When a project is created from modal
      * @param project
      * @param mode
      * @param isError
      */

     onAction = (project, mode = 'add', isError = false) => {
          let {projects} = this.state;
          let action = {};

          if (isError) {
               action = {
                    text: 'An error occurred while doing this action.',
                    level: 'danger'
               };

               return;
          }

          if (mode === 'add') {
               projects = [project, ...projects];

               action = {
                    text: 'Project successfully created.',
                    level: 'success'
               }
          } else {
               projects = projects.map(oldProject => {
                    if (oldProject.id === project.id) {
                         return project;
                    }

                    return oldProject;
               });

               action = {
                    text: 'Project successfully updated.',
                    level: 'success'
               }
          }

          this.setState({ projects, action });
     };

     /**
      * On project input
      * @param e
      */

     onSearch = e => {
          const {searchTimeout} = this.state;
          const {value} = e.target;

          if (searchTimeout) {
               clearTimeout(searchTimeout)
          }

          this.setState({
               searchTimeout: setTimeout(async () => {
                    await this.loadData(1, { search: value });
               }, 350)
          });
     };

     render() {
          const {projects, action, loading, hasMoreItems, page, filters} = this.state;

          return (
               <Layout>
                    <ProjectModal ref={modal => this.projectModal = modal} onAction={this.onAction}/>
                    <div className={'flex-center-sm mt-23-xs'}>
                         <h4>Projects</h4>
                         <div className={'ml-auto flex-center'}>
                              <div className={'input-search'}>
                                   <Input placeholder="Search" onChange={this.onSearch}/>
                                   <i className={'fas fa-search'}/>
                              </div>
                              <Button color={'success'} size={'sm'} onClick={() => this.projectModal.toggle(true)} className={'btn-add'}>
                                   <i className={'fas fa-plus'} />
                              </Button>
                         </div>
                    </div>
                    <hr className={'mb-33'}/>
                    {
                         action && (
                              <Alert color={action.level} toggle={this.closeAlert}>
                                   {action.text}
                              </Alert>
                         )
                    }
                    <Loading visible={loading}/>
                    {
                         !Boolean(projects.length) && !loading && (
                              <div>
                                   <p>There are no projects to display</p>
                                   <Button onClick={() => this.projectModal.toggle(true)}>Create project</Button>
                              </div>
                         )
                    }
                    {
                         Boolean(projects.length) && !loading && (
                              <Fragment>
                                   <Context.Consumer>
                                        {
                                             ({ user }) => (
                                                  <Table borderless striped={true} responsive={true}>
                                                       <thead>
                                                            <tr>
                                                                 <th>#</th>
                                                                 <th>Name</th>
                                                                 {user.access === ACCESS_ADMIN && <th>User</th>}
                                                                 <th>Status</th>
                                                            </tr>
                                                       </thead>
                                                       <tbody>
                                                       {
                                                            projects.map((project, key) => (
                                                                 <tr key={key}>
                                                                      <th scope="row">{key + 1}</th>
                                                                      <td>{project.name}</td>
                                                                      {user.access === ACCESS_ADMIN && <td>{get(project, 'user.name', '-')}</td>}
                                                                      <td>
                                                                           {
                                                                                (() => {
                                                                                     if (+project.processed === PROJECT_NOT_PROCESSED) {
                                                                                         return <Badge color={'primary'}>Not processed</Badge>
                                                                                     }

                                                                                     if (+project.processed === PROJECT_PENDING_PROCESS) {
                                                                                          return <Badge color="warning">Pending</Badge>
                                                                                     }

                                                                                     if (+project.processed === PROJECT_PROCESSED) {
                                                                                          return <Badge color="success">Processed</Badge>
                                                                                     }

                                                                                     return '-';
                                                                                })()
                                                                           }
                                                                      </td>
                                                                      <td>
                                                                           <Dropdown isOpen={project.toggled} toggle={() => this.toggle(project)} className={'actions-wrapper'}>
                                                                                <DropdownToggle>
                                                                                     <i className="fas fa-ellipsis-h actions"/>
                                                                                </DropdownToggle>
                                                                                <DropdownMenu>
                                                                                     <DropdownItem onClick={() => this.projectModal.toggle(true, project)}>
                                                                                          <i className="fas fa-edit"/>
                                                                                          Edit
                                                                                     </DropdownItem>
                                                                                     <DropdownItem onClick={() => this.delete(project)}>
                                                                                          <i className="fas fa-trash-alt"/> Delete
                                                                                     </DropdownItem>
                                                                                </DropdownMenu>
                                                                           </Dropdown>
                                                                      </td>
                                                                 </tr>
                                                            ))
                                                       }
                                                       </tbody>
                                                  </Table>
                                             )
                                        }
                                   </Context.Consumer>
                                   {
                                        hasMoreItems && (
                                             <div className={'text-center'}>
                                                  <Button color={`primary`} size={'sm'} className={'mt-23'} onClick={() => this.loadData(page + 1, filters)}>Load more</Button>
                                             </div>
                                        )
                                   }
                              </Fragment>
                         )
                    }
               </Layout>
          );
     }
}