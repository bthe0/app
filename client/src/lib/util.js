import React from 'react';

/**
 * Helper function for setting page title
 * @param title
 */

export const setTitle = title => {
     document.title = `App | ${title}`;
};


/**
 * Builds an error list
 * @param errors
 */

export const buildError = errors => {
     const returned = [];

     for (const label in errors) {
          if (!errors.hasOwnProperty(label)) {
               continue;
          }

          returned.push(
               <ul key={label}>
                    <span className={'error-label'}>{label}:</span>
                    {
                         errors[label].map((error, key) => (<li key={key}>{error}</li>))
                    }
               </ul>
          );
     }

     return (
          <div className={'errors-list'}>
               {returned}
          </div>
     );
};