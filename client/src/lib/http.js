import axios from 'axios';
import aesjs from 'aes-js';
import {API_URL, ENCRYPT_REQUESTS} from '../constants';

/**
 * AES Key
 * @type {number[]}
 */

const KEY_128 = [8, 6, 2, 3, 1, 6, 6, 1, 4, 5, 0, 4, 4, 0, 0, 4];

/**
 * Create the http client
 * @type {AxiosInstance}
 */

const client = axios.create({
     baseURL: API_URL,
     timeout: 10000,
     withCredentials: true,
     transformRequest: [
          (data) => {
               if (!data) {
                    return;
               }

               if (!ENCRYPT_REQUESTS) {
                    return JSON.stringify(data);
               }

               const textBytes = aesjs.utils.utf8.toBytes(JSON.stringify(data));
               const aesCtr = new aesjs.ModeOfOperation.ctr(KEY_128, new aesjs.Counter(5));
               const encryptedBytes = aesCtr.encrypt(textBytes);
               const encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);

               return encryptedHex;
          }
     ],
     transformResponse: [
          (data) => {
               try {
                   if (!ENCRYPT_REQUESTS) {
                       return JSON.parse(data);
                   }

                    const serverResponse = JSON.parse(data);

                    if (!serverResponse.result) {
                         throw new Error('Invalid server response.');
                    }

                    const encryptedBytes = aesjs.utils.hex.toBytes(serverResponse.result);
                    const aesCtr = new aesjs.ModeOfOperation.ctr(KEY_128, new aesjs.Counter(5));
                    const decryptedBytes = aesCtr.decrypt(encryptedBytes);
                    const decryptedText = aesjs.utils.utf8.fromBytes(decryptedBytes);

                    return JSON.parse(decryptedText);
               } catch(error) {
                    return {
                         errors: {
                              message: ['Invalid server response']
                         }
                    }
               }
          }
     ]
});

export default client;