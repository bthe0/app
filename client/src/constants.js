/**
 * User access levels
 * @type {number}
 */

export const ACCESS_USER = 0;
export const ACCESS_ADMIN = 1;


/**
 * Misc constants
 * @type {string}
 */

export const API_URL = 'https://localhost:3030/v1';
export const ENCRYPT_REQUESTS = true;

/**
 * Project status types
 * @type {number}
 */

export const PROJECT_NOT_PROCESSED = 0;
export const PROJECT_PENDING_PROCESS = 1;
export const PROJECT_PROCESSED = 2;